#!/usr/bin/env python3

import os
import redis
from flask import Flask, escape, request

app = Flask(__name__)
redis = redis.Redis(host=os.environ['REDIS_HOST'], port=6379, db=0)


@app.route('/')
def hello():
    query = request.args.get("q")
    if not query:
        return f"Hello Flask"

    redis.set(query, query.upper())
    value = redis.get(query)

    return f"{query}={value}"
