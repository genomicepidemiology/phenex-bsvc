#!/usr/bin/env ash

pip3 install -r /src/requirements.txt

flask run --host=0.0.0.0 --port=80
